import java.sql.Timestamp;

public class Counter extends Thread {

    Counter(String name){
        super(name);
    }

    public void run(){
        for(int i=0;i<7;i++){
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            System.out.println(getName()+"-"+timestamp);
            try {
                Thread.sleep((int)(2000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + "displayed all 7 messages ");
    }

    public static void main(String[] args) {
        Counter CThread1 = new Counter("CThread1");
        Counter CThread2 = new Counter("CThread2");

        CThread1.start();
        CThread2.start();
    }
}